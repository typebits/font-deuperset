# Deuperset

A 10x7 font created in the "Fonts.txt" workshop at the Free Culture Forum 2014,
facilitated by Manufactura Independente. It was designed and built in 4 hours.

Read more about the workshop at the [Type:Bits site](https://typebits.gitlab.io).

![Font preview](https://gitlab.com/typebits/font-deuperset/-/jobs/artifacts/master/raw/Deuperset-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-deuperset/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-deuperset/-/jobs/artifacts/master/raw/Deuperset-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-deuperset/-/jobs/artifacts/master/raw/Deuperset-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-deuperset/-/jobs/artifacts/master/raw/Deuperset-Regular.sfd?job=build-font)

## Authors

* Aurora Alonso, [@aurorispolaris](https://twitter.com/aurorispolaris)
* Alba Clemente, [@aclemnt](https://twitter.com/aclemnt)
* Sílvia Fabra, [@silviafabra](https://twitter.com/silviafabra)
* Nuría Fernández
* Maitane González
* Adrià Valls

## License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
